#!/usr/bin/env bash

source ./setup_zsh.sh

function setup_osx() {
  _brew
  _python
  _setup_zsh
}

function _brew() {
  if ! command -v brew &> /dev/null
  then
      echo "Brew could not be found installing"
      /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  fi

  if ! command -v brew &> /dev/null;
  then
    echo "Brew failed to install please install manually"
    exit
  else
    echo "Installing application using brew."
    xargs brew install < ./resources/brew-list.txt
  fi
}

function _python() {
  if ! command -v python &> /dev/null
  then
      echo "Python Not installed installing"
      brew install python
  fi

  if ! command -v pip3 &> /dev/null
  then
      echo "PIP3 Not installed"
  fi
}

function _awsmfa() {
  read -p "Do you want to install AWS MFA y/N? " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
      pip3 install aws-mfa
  fi
}