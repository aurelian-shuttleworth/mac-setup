### Useful plugins

* [docker](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/docker)
* [docker-compose](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/docker-compose)
* [emoji](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/emoji)
* [last-working-dir](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/last-working-dir)
* [OSX](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/osx)
* [zsh-interactive-cd](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/zsh-interactive-cd)

