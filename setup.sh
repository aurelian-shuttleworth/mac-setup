#!/usr/bin/env bash

source ./setup_osx.sh

if [ "$(uname)" == "Darwin" ]; then
    # Setup OSX
    setup_osx
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    echo "Linux is not supported"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    echo "Windows 32 Bit is not supported"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    echo "Windows 64 Bit is not supported"
fi

function _krypton() {
  if command -v kr &> /dev/null;
  then
    read -p "Configure Krypton ? y/N" -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      kr pair
    fi
  fi
}

#function _ask() {
#  read -p "Are you sure? " -n 1 -r
#  echo    # (optional) move to a new line
#  if [[ $REPLY =~ ^[Yy]$ ]]
#  then
#      # do dangerous stuff
#  fi
#}