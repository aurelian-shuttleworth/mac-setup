[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases

# Tools
alias ls='ls -G'                              # colorize `ls` output
alias zshreload='source ~/.zshrc'             # reload ZSH
alias shtop='sudo htop'                       # run `htop` with root rights
alias grep='grep --color=auto'                # colorize `grep` output
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias less='less -R'
alias g='git'

alias decode='pbpaste | base64 --decode | pbcopy' #decode base64 from copied text

alias rm='rm -i'                              # confirm removal
alias cp='cp -i'                              # confirm copy
alias mv='mv -i'                              # confirm move
alias cal='gcal --starting-day=1'             # print simple calendar for current month
alias weather='curl v2.wttr.in'               # print weather for current location (https://github.com/chubin/wttr.in)
alias edita='subl ~/.zsh/aliases.zsh'             # Edit alias in sublime

alias tolower="tr '[:upper:]' '[:lower:]'"
alias toupper="tr '[:upper:]' '[:lower:]'"

alias transcribe="gcloud alpha ml speech recognize-long-running --language-code="en-ZA" --encoding="ogg-opus" --sample-rate=16000 --enable-automatic-punctuation --interaction-type="phone-call" --recording-device-type="smartphone""

alias cleancopy="ansifilter --rtf | pbcopy -Prefer rtf"

eval $(thefuck --alias)

# Terraform Alias

alias tf='terraform'
alias tfi='terraform init'
alias tfa='terraform apply'
alias tfd='terraform destroy'
alias tfp='terraform plan'


alias gshell='gcloud alpha cloud-shell ssh'       # ssh into google cloud shell

# Go Alias

alias gor='go run'
alias gob='go build'
alias goi='go install'
alias setgopath='export GOPATH=$PWD'
alias setgoroot='export GOROOT=$PWD'
alias setgobin='export GOBIN=$PWD'

# Ranger Alias

alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'

# AWS
alias awsmfalogin="aws-mfa --device arn:aws:iam::135185517067:mfa/aurelian.shuttleworth@sprinthive.com --profile sprinthive --duration 28800"