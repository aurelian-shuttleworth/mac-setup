function _setup_zsh() {
  if ! command -v zsh &> /dev/null
  then
      echo "ZSH Not installed installing"
      brew install zsh
  fi

  if ! command -v omz &> /dev/null
  then
      echo "ohmyzsh Not installed installing"
      sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  fi

  omz update

  echo "installing PowerLevel9k"
  git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

  echo "installing Powerline fonts"
  git clone https://github.com/powerline/fonts.git ./tmp/
  /bin/bash ./tmp/fonts/install.sh
  rm -rf ./tmp/fonts

}